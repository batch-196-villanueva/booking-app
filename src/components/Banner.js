import {Row, Col, Button} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

export default function Banner({bannerProp}){

	const{title,description,url,btn} = bannerProp

	const navigate = useNavigate()

	// redirects to corresponding pages
	function redirect(){
		navigate(`/${url}`, {replace:true})
	}

	return (
		// banners to show
		<>		
			<Row>
				<Col className ="p-5">
					<h1>{title}</h1>
					<p>{description}</p>
					<Button variant="primary" onClick={redirect}>{btn}</Button>
				</Col>
			</Row>
		</>
	)	
};
