import coursesData from '../data/coursesData'
import CourseCards from '../components/CourseCard';
import { useState, useEffect } from 'react';

export default function Courses(){
    // console.log(coursesData);
    // console.log(coursesData[0]);
    const [courses,setCourses] = useState([])

    // const courses = coursesData.map(course =>
    //     {return(
    //         <CourseCards key={course.id} courseProp= {course}/>            
    //     )}
        
    // )
    useEffect(() => {
        fetch("http://localhost:4000/courses")
    .then(res => res.json())
    .then(data => {
        console.log(data);
        setCourses(data.map(course => {
            return(
            <CourseCards key={course._id} courseProp= {course}/>            
            )
        }))
    })
    }, [])

    return(
        <>
            <h1>Available Courses</h1>
            {courses}
        </>
    )
}
