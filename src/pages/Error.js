import Banner from '../components/Banner';

export default function error(){
    // return (
    //     <>
    //     <Banner bannerProp="error"/>
    //     </>
    // )
    const data = {
        title: "404 - Page Not Found",
        description: "The page you are looking for cannot be found.",
        url: "",
        btn: "Back to Home"
    }
        return (
        <>
        <Banner bannerProp={data}/>
        </>
    )

}