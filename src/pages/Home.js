import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCards from '../components/CourseCard';

export default function Home(){
	// using props
	const data = {
        title: "B196 Booking App",
        description: "Opportunities for everyone, everywhere!",
        url: "courses",
        btn: "Enroll Now"
    }
	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>

	)
}
