// mock database

const coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Cupidatat nulla proident dolore proident minim nostrud esse ad commodo consectetur excepteur incididunt ex quis. Cillum eu voluptate consectetur mollit ut veniam non aute ipsum labore duis exercitation id magna. Consectetur cillum dolor voluptate cupidatat velit do elit magna fugiat cillum ex incididunt.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python-Django",
        description: "Quis id veniam laboris anim dolor aute nulla et exercitation consequat sint velit. Reprehenderit dolore aute eu dolore veniam sint nostrud eiusmod sint elit. Lorem qui in do aute non sint elit ipsum tempor sunt exercitation. Sint Lorem velit aute consequat proident laborum eu nostrud laboris aliquip in est non. Aliquip exercitation tempor anim esse consectetur sint ipsum Lorem pariatur.",
        price: 55000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Pariatur magna eiusmod culpa culpa aute. Aute adipisicing laboris ad aute enim consequat et minim incididunt irure eu culpa est ex. Lorem deserunt amet qui culpa aute eiusmod culpa ipsum irure anim quis. Reprehenderit sunt ex non eiusmod sunt sint veniam dolore esse cupidatat ullamco culpa pariatur nisi. Nulla occaecat non dolor sit do sint quis minim minim qui.",
        price: 55000,
        onOffer: true
    }
]

export default coursesData;